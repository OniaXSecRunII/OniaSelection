//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sat Jan 12 22:20:01 2019 by ROOT version 6.14/04
// from TTree tree/myTree
// found on file: /eos/atlas/user/b/bchargei/BLS/data_BPHY1/data15p/user.bchargei.data15_13TeV.periodD.physics_Main.DAOD_BPHY1.08_01_2019.v02.root_MYSTREAM/user.bchargei.16671825.MYSTREAM._000001.root
//////////////////////////////////////////////////////////

#ifndef NtupleMaker_h
#define NtupleMaker_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>

// Headers needed by this particular selector
#include <vector>
#include <TString.h>
#include <TProofOutputFile.h>
#include <TSystem.h>
#include "/afs/cern.ch/work/b/bchargei/private/JPsi/JpsiAnalysis/Tools/BPhysTrigCorrection/BPhysTrigCorrection.cxx"

#include "TLorentzVector.h"


class NtupleMaker : public TSelector {
	public:
		const std::vector<double> bins_pt = {
			8.0,9.0,10.,11.,12.,13.,14.,15,16,17,18,
			19,20,22,24,26,28,30,35,40,45,50,56.5,60,
			70,80,90,100,120,140,160,180,200,240,300,360};
		const std::vector<double> bins_y = {0, 0.75, 1.5, 2.00};
		std::vector<TTree*>  fTrees;     //! The output tree vector
		TProofOutputFile    *fProofFile; //! For dataset creation in create mode
		TFile               *fFile;      //! Output file in create mode
		//Output tree leaves
		int f_runNumber = -1;
		int f_eventNumber = -1;
		double f_m = -1;
		double f_m_CB = -1;
		double f_m_Ref = -1;
		double f_m_Trk = -1;
		double f_merr = -1;
		double f_tau = -1;
		double f_tauconst = -1;
		//double f_taupt2 = -1;
		//double f_taua0 = -1;
		double f_tauerr = -1;
		double f_Lxy = -1;
		double f_pt = -1;
		double f_y = -1;
		double f_dr = -1;
		double f_pt1 = -1;
		double f_pt2 = -1;
		double f_q1 = -999;
		double f_q2 = -999;
		double f_qeta1 = -1;
		double f_qeta2 = -1;
		double f_qphi1 = -1;
		double f_qphi2 = -1;
		double f_cosTheta_hx = -1;
		double f_phi_hx = -1;
		double f_maxd0 = -1;
		int f_passTrigger = -1;
		int f_mu0_match = -1;
		int f_mu1_match = -1;
		//double f_weight = -1;
		//double f_weight1S = -1;
		//double f_weight2S = -1;
		//double f_weight_eff = -1;
		//double f_weight_acc = -1;
		//double f_weight_acc_jpsi = -1;
		//double f_weight_acc_psi2s = -1;

		double f_truth_pt = -1;
		double f_truth_eta = -1;
		double f_truth_phi = -1;
		double f_truth_m = -1;
		double f_truth_pt1 = -1;
		double f_truth_pt2 = -1;
		double f_truth_dr = -1;

		double f_weight_Lxy = -1;
		double f_weight_reco = -1;
		double f_weight_cmumu = -1;
		double f_weight_corrTau = -1;
		double f_weight_trigSf = -1;
		double f_weight_trigEff = -1;
		double f_weight_trig = -1;
		double f_weight_trig_single = -1;
		double f_eff_trig_single_mu0 = -1;
		double f_eff_trig_single_mu1 = -1;
		double f_eff_trig_single_mu0_up = -1;
		double f_eff_trig_single_mu0_down = -1;
		double f_eff_trig_single_mu1_up = -1;
		double f_eff_trig_single_mu1_down = -1;
		double f_eff_reco_mu0 = -1;
		double f_eff_reco_mu1 = -1;
		double f_eff_reco_mu0_up = -1;
		double f_eff_reco_mu1_up = -1;
		double f_eff_reco_mu0_down = -1;
		double f_eff_reco_mu1_down = -1;

		//Options, parameters
		int f_data; //!
		TH1F* f_h_LxyCorr; //!
		TFile* f_trigFile; //!
		TFile* f_recoFile_low; //!
		TFile* f_recoFile_hi; //!
		int f_triggerNumber; //!
		TString f_output; //!
		BPhysTrigCorrection* f_triggCorrection; //!


		TTreeReader     fReader;  //!the tree reader
		TTree          *fChain = 0;   //!pointer to the analyzed TTree or TChain

		// Readers to access the data (delete the ones you do not need).
		TTreeReaderValue<Int_t> RunNumber = {fReader, "RunNumber"};
		TTreeReaderValue<Int_t> EventNumber = {fReader, "EventNumber"};
		TTreeReaderValue<Int_t> LumiBlock = {fReader, "LumiBlock"};
		TTreeReaderValue<Int_t> mcChannelNumber = {fReader, "mcChannelNumber"};
		TTreeReaderValue<Int_t> EventTrigger = {fReader, "EventTrigger"};
		TTreeReaderArray<float> PS = {fReader, "PS"};
		TTreeReaderArray<float> Truth_Parent_PdgId = {fReader, "Truth_Parent_PdgId"};
		TTreeReaderArray<float> Truth_Parent_Pt = {fReader, "Truth_Parent_Pt"};
		TTreeReaderArray<float> Truth_Parent_Eta = {fReader, "Truth_Parent_Eta"};
		TTreeReaderArray<float> Truth_Parent_Phi = {fReader, "Truth_Parent_Phi"};
		TTreeReaderArray<float> Truth_Parent_Mass = {fReader, "Truth_Parent_Mass"};
		TTreeReaderArray<float> Truth_Onia_PdgId = {fReader, "Truth_Onia_PdgId"};
		TTreeReaderArray<float> Truth_Onia_Pt = {fReader, "Truth_Onia_Pt"};
		TTreeReaderArray<float> Truth_Onia_Eta = {fReader, "Truth_Onia_Eta"};
		TTreeReaderArray<float> Truth_Onia_Phi = {fReader, "Truth_Onia_Phi"};
		TTreeReaderArray<float> Truth_Onia_Mass = {fReader, "Truth_Onia_Mass"};
		TTreeReaderArray<float> Truth_Onia_Vertex_X = {fReader, "Truth_Onia_Vertex_X"};
		TTreeReaderArray<float> Truth_Onia_Vertex_Y = {fReader, "Truth_Onia_Vertex_Y"};
		TTreeReaderArray<float> Truth_Onia_Vertex_Z = {fReader, "Truth_Onia_Vertex_Z"};
		TTreeReaderArray<float> Truth_MuMinus_Pt = {fReader, "Truth_MuMinus_Pt"};
		TTreeReaderArray<float> Truth_MuMinus_Eta = {fReader, "Truth_MuMinus_Eta"};
		TTreeReaderArray<float> Truth_MuMinus_Phi = {fReader, "Truth_MuMinus_Phi"};
		TTreeReaderArray<float> Truth_MuPlus_Pt = {fReader, "Truth_MuPlus_Pt"};
		TTreeReaderArray<float> Truth_MuPlus_Eta = {fReader, "Truth_MuPlus_Eta"};
		TTreeReaderArray<float> Truth_MuPlus_Phi = {fReader, "Truth_MuPlus_Phi"};
		TTreeReaderValue<Float_t> Truth_PV_Z = {fReader, "Truth_PV_Z"};
		TTreeReaderValue<Float_t> Truth_PV_X = {fReader, "Truth_PV_X"};
		TTreeReaderValue<Float_t> Truth_PV_Y = {fReader, "Truth_PV_Y"};
		TTreeReaderValue<Float_t> PV_Z = {fReader, "PV_Z"};
		TTreeReaderValue<Float_t> PV_X = {fReader, "PV_X"};
		TTreeReaderValue<Float_t> PV_Y = {fReader, "PV_Y"};
		TTreeReaderArray<float> DiMuon_Mass = {fReader, "DiMuon_Mass"};
		TTreeReaderArray<float> DiMuon_MassErr = {fReader, "DiMuon_MassErr"};
		TTreeReaderArray<float> DiMuon_Pt = {fReader, "DiMuon_Pt"};
		TTreeReaderArray<float> DiMuon_Rapidity = {fReader, "DiMuon_Rapidity"};
		TTreeReaderArray<float> DiMuon_ChiSq = {fReader, "DiMuon_ChiSq"};
		TTreeReaderArray<float> DiMuon_Vertex_X = {fReader, "DiMuon_Vertex_X"};
		TTreeReaderArray<float> DiMuon_Vertex_Y = {fReader, "DiMuon_Vertex_Y"};
		TTreeReaderArray<float> DiMuon_Vertex_Z = {fReader, "DiMuon_Vertex_Z"};
		TTreeReaderArray<float> Tau_ConstMass_MaxSumPt2 = {fReader, "Tau_ConstMass_MaxSumPt2"};
		TTreeReaderArray<float> TauErr_ConstMass_MaxSumPt2 = {fReader, "TauErr_ConstMass_MaxSumPt2"};
		TTreeReaderArray<float> Tau_InvMass_MaxSumPt2 = {fReader, "Tau_InvMass_MaxSumPt2"};
		TTreeReaderArray<float> TauErr_InvMass_MaxSumPt2 = {fReader, "TauErr_InvMass_MaxSumPt2"};
		TTreeReaderArray<float> Tau_ConstMass_Min_A0 = {fReader, "Tau_ConstMass_Min_A0"};
		TTreeReaderArray<float> TauErr_ConstMass_Min_A0 = {fReader, "TauErr_ConstMass_Min_A0"};
		TTreeReaderArray<float> Tau_InvMass_Min_A0 = {fReader, "Tau_InvMass_Min_A0"};
		TTreeReaderArray<float> TauErr_InvMass_Min_A0 = {fReader, "TauErr_InvMass_Min_A0"};
		TTreeReaderArray<float> Tau_ConstMass_Min_Z0 = {fReader, "Tau_ConstMass_Min_Z0"};
		TTreeReaderArray<float> TauErr_ConstMass_Min_Z0 = {fReader, "TauErr_ConstMass_Min_Z0"};
		TTreeReaderArray<float> Tau_InvMass_Min_Z0 = {fReader, "Tau_InvMass_Min_Z0"};
		TTreeReaderArray<float> TauErr_InvMass_Min_Z0 = {fReader, "TauErr_InvMass_Min_Z0"};
		TTreeReaderArray<float> Lxy_MaxSumPt2 = {fReader, "Lxy_MaxSumPt2"};
		TTreeReaderArray<float> LxyErr_MaxSumPt2 = {fReader, "LxyErr_MaxSumPt2"};
		TTreeReaderArray<float> Lxy_Min_A0 = {fReader, "Lxy_Min_A0"};
		TTreeReaderArray<float> LxyErr_Min_A0 = {fReader, "LxyErr_Min_A0"};
		TTreeReaderArray<float> Lxy_Min_Z0 = {fReader, "Lxy_Min_Z0"};
		TTreeReaderArray<float> LxyErr_Min_Z0 = {fReader, "LxyErr_Min_Z0"};
		TTreeReaderArray<int> DiMuon_TriggMatch = {fReader, "DiMuon_TriggMatch"};
		TTreeReaderArray<float> Muon_Phi_hx = {fReader, "Muon_Phi_hx"};
		TTreeReaderArray<float> Muon_CosTheta_hx = {fReader, "Muon_CosTheta_hx"};
		TTreeReaderArray<float> Muon_Phi_hxOLD = {fReader, "Muon_Phi_hxOLD"};
		TTreeReaderArray<float> Muon_CosTheta_hxOLD = {fReader, "Muon_CosTheta_hxOLD"};
		TTreeReaderArray<TLorentzVector> Muon0_RefTrack_p4 = {fReader, "Muon0_RefTrack_p4"};
		TTreeReaderArray<TLorentzVector> Muon0_Track_p4 = {fReader, "Muon0_Track_p4"};
		TTreeReaderArray<TLorentzVector> Muon0_CBTrack_p4 = {fReader, "Muon0_CBTrack_p4"};
		TTreeReaderArray<int> Muon0_Charge = {fReader, "Muon0_Charge"};
		TTreeReaderArray<int> Muon0_Quality = {fReader, "Muon0_Quality"};
		TTreeReaderArray<int> Muon0_TriggMatch = {fReader, "Muon0_TriggMatch"};
		TTreeReaderArray<TLorentzVector> Muon1_RefTrack_p4 = {fReader, "Muon1_RefTrack_p4"};
		TTreeReaderArray<TLorentzVector> Muon1_Track_p4 = {fReader, "Muon1_Track_p4"};
		TTreeReaderArray<TLorentzVector> Muon1_CBTrack_p4 = {fReader, "Muon1_CBTrack_p4"};
		TTreeReaderArray<int> Muon1_Charge = {fReader, "Muon1_Charge"};
		TTreeReaderArray<int> Muon1_Quality = {fReader, "Muon1_Quality"};
		TTreeReaderArray<int> Muon1_TriggMatch = {fReader, "Muon1_TriggMatch"};
		TTreeReaderArray<float> Muon0_RecoEffTight = {fReader, "Muon0_RecoEffTight"};
		TTreeReaderArray<float> Muon1_RecoEffTight = {fReader, "Muon1_RecoEffTight"};
		TTreeReaderArray<float> Muon0_RecoEffMedium = {fReader, "Muon0_RecoEffMedium"};
		TTreeReaderArray<float> Muon1_RecoEffMedium = {fReader, "Muon1_RecoEffMedium"};
		TTreeReaderArray<float> Muon0_RecoEffLoose = {fReader, "Muon0_RecoEffLoose"};
		TTreeReaderArray<float> Muon1_RecoEffLoose = {fReader, "Muon1_RecoEffLoose"};
		TTreeReaderArray<float> Muon0_RecoEffLowPt = {fReader, "Muon0_RecoEffLowPt"};
		TTreeReaderArray<float> Muon1_RecoEffLowPt = {fReader, "Muon1_RecoEffLowPt"};
		TTreeReaderArray<float> Muon0_mu50TriggEffTight = {fReader, "Muon0_mu50TriggEffTight"};
		TTreeReaderArray<float> Muon1_mu50TriggEffTight = {fReader, "Muon1_mu50TriggEffTight"};
		TTreeReaderArray<float> Muon0_mu50TriggEffMedium = {fReader, "Muon0_mu50TriggEffMedium"};
		TTreeReaderArray<float> Muon1_mu50TriggEffMedium = {fReader, "Muon1_mu50TriggEffMedium"};
		TTreeReaderArray<float> Muon1_mu50TriggEffLoose = {fReader, "Muon1_mu50TriggEffLoose"};
		TTreeReaderArray<float> Muon0_mu50TriggEffLoose = {fReader, "Muon0_mu50TriggEffLoose"};


		NtupleMaker();
		virtual ~NtupleMaker();
		virtual Int_t   Version() const { return 2; }
		virtual void    Begin(TTree *tree);
		virtual void    SlaveBegin(TTree *tree);
		virtual void    Init(TTree *tree);
		virtual Bool_t  Notify();
		virtual Bool_t  Process(Long64_t entry);
		virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
		virtual void    SetOption(const char *option) { fOption = option; }
		virtual void    SetObject(TObject *obj) { fObject = obj; }
		virtual void    SetInputList(TList *input) { fInput = input; }
		virtual TList  *GetOutputList() const { return fOutput; }
		virtual void    SlaveTerminate();
		virtual void    Terminate();

		TString getDataPeriod(unsigned int runNumber, unsigned year);
		void getTrigEfficiency(Double_t& eff, TLorentzVector& mu, TString systematic, unsigned int runNumber, unsigned year);
		void getRecoEfficiency(Double_t& eff, TLorentzVector& mu, TString systematic);
		double getLxyWeight(double Lxy);

		ClassDef(NtupleMaker,0);

};

#endif

#ifdef NtupleMaker_cxx
void NtupleMaker::Init(TTree *tree)
{
	// The Init() function is called when the selector needs to initialize
	// a new tree or chain. Typically here the reader is initialized.
	// It is normally not necessary to make changes to the generated
	// code, but the routine can be extended by the user if needed.
	// Init() will be called many times when running on PROOF
	// (once per file to be processed).

	fReader.SetTree(tree);
}

Bool_t NtupleMaker::Notify()
{
	// The Notify() function is called when a new file is opened. This
	// can be either for a new TTree in a TChain or when when a new TTree
	// is started when using PROOF. It is normally not necessary to make changes
	// to the generated code, but the routine can be extended by the
	// user if needed. The return value is currently not used.

	return kTRUE;
}


#endif // #ifdef NtupleMaker_cxx
