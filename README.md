# OniaSelection

Scripts to prepare output of OniaAna for fitting procedure in RooFit.

To run the code, use following:
```bash
root -l -b run.C\(\"data15.txt\"\) 
#Change data15.txt as needed
```
This will produce an output file with separate tree for every pt and |y| bins 
with selected variables.

To produce the RooDatasets for fitting procedure, use the following commands:

```bash
root -l ntuple_charm_data15.root RooDataMaker.C
```